import json
import requests
import threading

from flask import Flask, render_template, jsonify
from log import logging
from multiprocessing.dummy import Pool as ThreadPool
from config import refresh_interval, filename, site_down
from socket import gaierror, gethostbyname
from time import gmtime, strftime
from urllib.parse import urlparse

# initialize the logger
logger = logging.getLogger('app')


def is_reachable(url) -> bool:
    """ Function to check if the hostname/ip exists by checking the DNS entry."""
    try:
        # print('check if reachable')
        ip = gethostbyname(url)
        # print("ip {%s} for url {%s}", ip, url)
    except (gaierror):
        return False
    else:
        return True


def get_status_code(url) -> str:
    """ This function returns the status code of the url.
    this function will also get te elapsed time and error message from the response"""
    try:
        response = requests.get(url)
        
        status_code = response.status_code
        elapsed_time = response.elapsed.total_seconds()
        logger.info(f'success response from url {url} with status code {status_code} in {elapsed_time} seconds. \n')
        logger.warning(f'success response from url {url} with status code {status_code} in {elapsed_time} seconds')
        
        return status_code
    except requests.ConnectionError as err:
        logger.error(f'Error connecting to url {url} with error {err} \n')
        # print ("error %s ", err)
        return site_down
    except requests.HTTPError as http_err:
        logger.error(f'HTTP error occurred in url {url} with error {http_err} \n')
    except Exception as exp_err:
        logger.error(f'Exception error occurred: {exp_err}') 


def check_single_url(url) -> str:
    """This function checks a given url and if connectable returns
    the status code, else returns UNREACHABLE."""
    if is_reachable(urlparse(url).hostname) == True:
        return str(get_status_code(url))
    else:
        return site_down


def check_multiple_urls() -> dict:
    """ This function checks through urls specified in the checkurls.json file
    and returns their statuses as a dictionary every 60s."""
    statuses = {}
    temp_list_urls = []
    temp_list_statuses = []
    global returned_statuses
    global last_update_time
    t = threading.Timer
    t(refresh_interval, check_multiple_urls).start()
    for group, urls in checkurls.items():
        for url in urls:
            # print('sssss')
            temp_list_urls.append(url)
    pool = ThreadPool(8)
    temp_list_statuses = pool.map(check_single_url, temp_list_urls)
    for i in range(len(temp_list_urls)):
        statuses[temp_list_urls[i]] = temp_list_statuses[i]
    last_update_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    returned_statuses = statuses


app = Flask(__name__)


@app.route("/", methods=["GET"])
def display_returned_statuses():
    return render_template(
        'returned_statuses.html',
        returned_statuses = returned_statuses,
        checkurls = checkurls,
        last_update_time = last_update_time
        )


@app.route("/api", methods=["GET"])
def display_returned_api():
    return jsonify(
        returned_statuses
        ),200


with open(filename) as f:
    checkurls = json.load(f)
returned_statuses = {}
last_update_time = 'time string'


if __name__ == '__main__':
    check_multiple_urls()
    app.run()
