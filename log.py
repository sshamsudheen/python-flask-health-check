import logging


# log file name
log_file_name = 'app.log'

# log file mode (a,w)
log_file_mode = 'a'

# log format
log_format = '%(asctime)s - %(message)s'

# log date format 
log_date_format = '%d-%b-%y %H:%M:%S'

logging.basicConfig(filename=log_file_name, filemode=log_file_mode, format=log_format, datefmt=log_date_format, level=logging.INFO)
