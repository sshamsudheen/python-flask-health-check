# Interval to refresh status codes in seconds
refresh_interval = 60.0

# File containing list of urls to check in json format.
filename = 'checkurls.json'

# Message to display if sites are not connectable
site_down = 'UNREACHABLE'
