<h1>Requirments </h1>

- UBUNTU 16.04 (OS)

- Python 3.7.7

- Flask 1.1.1

- Werkzeug 0.16.1


<h1>Instructions</h1>

1. Clone the repository
2. Install Flask ` pip install -r reequirements.txt`
3. Change checkurls.json file and add the URL's that need to be checked (note: use the existing format to add the url)
4. Change config.py file if you want the refreshing interval to be changed and message of site not reachable
5. Go to your project directory and execute
    `python3 app.py`

6. Above command will gives you the below output
``` shell
sshamsudheen: Shams/personal/healthcheck$ python3 app.py
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
```
7. Then go to your browser and type http://localhost:5000/ you will be able to save the web page as below

![Alt text](https://gitlab.com/sshamsudheen/python-flask-health-check/-/blob/master/screenshot.PNG)

8. Logs can be found in app.log
